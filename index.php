<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/custome.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<!-- div for top advertise -->
		<div class="row">
		<div class="col-xs-12 top-advertise-section">
		    <!-- advertise image here -->
		</div>
		</div>
        
    
		<!-- navigation	 -->
		<nav class="navbar navbar-expand-md navbar-light sps sps--abv" id='nav'>
		  <div class="container">
		  	
		  	  <a class="navbar-brand d-block " href="#home">Doe's</a>

		  	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navContent"  aria-expanded="true" aria-label="Toggle navigation">
		      <span class="navbar-toggler-icon"></span>
		      </button>

		  	  <div class="collapse navbar-collapse " id="navContent">
		    		<ul class="navbar-nav nav-fade-border mx-auto">
		    			<li class="nav-item"><a class='nav-link'>HOME</a></li>
		    			<li class="nav-item"><a class='nav-link'>About</a></li>
		                <li class="nav-item"><a class='nav-link'>Services</a></li>
		                <li class="nav-item"><a class='nav-link'>Resume</a></li>
		    			<li class="nav-item dropdown">
		    				<a class='nav-link' href="#" id="navbardrop" data-toggle="dropdown">Portfolio</a>
		    				<div class="dropdown-menu">
						        <a class="dropdown-item" href="#">Link 1</a>
						        <a class="dropdown-item" href="#">Link 2</a>
						        <a class="dropdown-item" href="#">Link 3</a>
					      </div>
		    			</li>
		    		</ul>
		      </div>

		       <form class="form-inline" action="/action_page.php">
			    <input class="form-control mr-sm-2" type="text" placeholder="Search">
			    <button class="btn btn-success" type="submit">Search</button>
			  </form>
			</div>
		  
		</nav>
		<!-- end nav -->

		<div class="row">
			<!-- for left main-content	 -->
			<div class="col-md-7">
				<!-- main-image -->
				<div class="row">
					<div class="col-md-12" style="border:1px solid red; min-height: 300px;">
					</div>
			    </div>
                <div class="row" style="margin-top: 10px;margin-bottom: 10px;">
	                <div class="col-md-7" style="border:1px solid red; min-height: 300px;">
					</div>

					<div class="col-md-5" style="border:1px solid red; min-height: 300px;">
					</div>
				</div>

			</div>
			<!-- for product and video view -->
			<div class="col-md-3">
				<div class="row">
					<div class="col-md-12" style="border:1px solid red; min-height: 300px;">
					</div>
			    </div>
			    <div class="row" style="margin-top: 10px;">
					<div class="col-md-12" style="border:1px solid red; min-height: 150px;">
					</div>

					<div class="col-md-12" style="border:1px solid red; min-height: 150px;">
					</div>

			    </div>
			</div>
			<!-- for right advertise -->
			<div class="col-md-2">
				<div class="row">
					<div class="col-md-12" style="border:1px solid red; min-height: 610px;">
					</div>
			    </div>
			</div>
		</div>

		
	</div>
</body>
</html>